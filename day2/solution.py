# -*- coding: utf-8 -*-
import time
from common import fetchinput

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  #part 1
  commands = raw.splitlines()
  pos = 0
  depth = 0
  for c in commands:
    command, val = c.split(" ")
    val = int(val)
    if command == "forward":
      pos += val
    elif command == "down":
      depth += val
    elif command == "up":
      depth -= val
  print("part 1: ", pos * depth)
  
  #part 2
  pos2 = 0
  depth2 = 0
  aim = 0
  for c in commands:
    command, val = c.split(" ")
    val = int(val)
    if command == "forward":
      pos2 += val
      depth2 += aim * val
    elif command == "down":
      aim += val
    elif command == "up":
      aim -= val
  print("part 2: ", pos2 * depth2)
  
  t1 = time.perf_counter()
  print(t1-t0)
  