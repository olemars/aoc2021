# -*- coding: utf-8 -*-
import time
from common import fetchinput
from collections import defaultdict

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  rawx = raw[raw.index("x")+2:raw.index(",")]
  rawy = raw[raw.index("y")+2:]
  xrange = tuple(int(v) for v in rawx.split(".."))
  yrange = tuple(int(v) for v in rawy.split(".."))
  
  #part 1
  vely = max([abs(v) for v in yrange]) - 1
  maxy = int((vely * (vely + 1)) / 2)
  velx = None
  for x in range(vely):
    if xrange[0] <= int((x * (x + 1)) / 2) <= xrange[1]:
      velx = x
      break
  print(f"Part 1: Max altitude {maxy} at ({velx},{vely})")
  
  #part 2
  stepx = defaultdict(list)
  limx = []
  for x in range(xrange[1], 0, -1):
    if int((x * (x + 1)) / 2) < xrange[0]:
      break
    vx = x
    step = 0
    while vx <= xrange[1]:
      if step >= x - 1 and xrange[0] <= int((x * (x + 1)) / 2) <= xrange[1]:
        limx.append(x)
        break
      step += 1
      if xrange[0] <= vx <= xrange[1]:
        stepx[step].append(x)
      vx += x - step
      
  stepy = defaultdict(list)
  for y in range(abs(yrange[0]), -(abs(yrange[0])+1), -1):
    vy = y
    step = 0
    while vy >= yrange[0]:
      step += 1
      if yrange[0] <= vy <= yrange[1]:
        stepy[step].append(y)
      vy += y - step
  
  targetvels = set()
  for s in set(stepx.keys()).intersection(set(stepy.keys())):
    [targetvels.add((x, y)) for x in stepx[s] for y in stepy[s]]
  for s in set(stepy.keys()).difference(set(stepx.keys())):
    [targetvels.add((x, y)) for x in limx for y in stepy[s]]
    
  print("Part 2: ", len(targetvels))
  
  t1 = time.perf_counter()
  print(t1-t0)
  