# -*- coding: utf-8 -*-
import time
from common import fetchinput
from collections import deque

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  grid = [[int(v) for v in l] for l in raw.splitlines()]
  ydim = len(grid)
  xdim = len(grid[0])
  neighbors = [(-1,-1), (-1,0), (-1,1), (0,-1), (0,1), (1,-1), (1,0), (1,1)]
    
  #part 1
  maxsteps = 1000
  part2 = None
  total = 0
  flashq = deque()
  flashedq = {}
  s = 1
  while part2 is None and s <= maxsteps:
    flashq.clear()
    flashedq.clear()
    for y in range(ydim):
      for x in range(xdim):
        if grid[y][x] == 9:
          flashq.append((y, x))
        grid[y][x] += 1
    
    while len(flashq) > 0:
      total += 1
      fy, fx = flashq.popleft()
      flashedq[(fy, fx)] = True
      for dy, dx in neighbors:
        nx = fx + dx
        ny = fy + dy
        if 0 <= nx < xdim and 0 <= ny < ydim:
          if grid[ny][nx] == 9:
            flashq.append((ny, nx))
          grid[ny][nx] += 1
    if len(flashedq) == xdim * ydim:
      part2 = s
    if s == 100:
      print("Part 1: ", total)
    for y,x in flashedq.keys():
      grid[y][x] = 0
    s += 1
    
  #part 2
  print("Part 2: ", part2)
    
  t1 = time.perf_counter()
  print(t1-t0)
  