# -*- coding: utf-8 -*-
import time
from common import fetchinput

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  values = [int(d) for d in raw.split("\n") ]
  
  #part 1
  answer1 = sum([(v1 - v0) > 0 for v0, v1 in zip(values[:-1], values[1:])])      
  print("part 1: ", answer1)
  
  #part 2  
  sums = [sum(v) for v in zip(values[:-2], values[1:-1], values[2:])]
  answer2 = sum([(v1 - v0) > 0 for v0, v1 in zip(sums[:-1], sums[1:])])
  print("part 2: ", answer2)
  
  t1 = time.perf_counter()
  print("time: " + str(t1-t0) + "s")