# -*- coding: utf-8 -*-
import time
from common import fetchinput
from math import floor

def rd(n):
  return floor(n + 0.5)

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  val = raw.splitlines()
  
  #part 1
  numval = len(val)
  numparams = len(val[0])
  sums = [0] * numparams
  
  for v in val: 
    for i in range(numparams):
      sums[i] += int(v[i])
  gamma = int("".join([str(rd(p / numval)) for p in sums]), 2)
  epsilon = gamma ^ int("1" * numparams, 2)
  
  print("part 1: ", gamma * epsilon)
  
  #part 2
  o2list = val[:]
  co2list = val[:]
  o2sum = sums[:]
  co2sum = sums[:]
  for i in range(numparams):
    if len(o2list) > 1:
      mostcommon = rd(o2sum[i] / len(o2list))
      removed = [v for v in o2list if int(v[i]) != mostcommon]
      o2list = [v for v in o2list if int(v[i]) == mostcommon]
      for p in range(numparams):
        for r in removed:
          o2sum[p] -= int(r[p])
    if len(co2list) > 1:
      mostcommon = rd(co2sum[i] / len(co2list))
      removed = [v for v in co2list if int(v[i]) == mostcommon]
      co2list = [v for v in co2list if int(v[i]) != mostcommon]
      for p in range(numparams):
        for r in removed:
          co2sum[p] -= int(r[p])
  if len(o2list) == 1 and len(co2list) == 1:
    print("part 2: ", int(o2list[0], 2) * int(co2list[0], 2))  
  
  t1 = time.perf_counter()
  print(t1-t0)
  