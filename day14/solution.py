# -*- coding: utf-8 -*-
import time
from common import fetchinput
from collections import Counter

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  template, rules = raw.split("\n\n")
  rulebook = { pair: insert for pair, insert in [r.split(" -> ") for r in rules.splitlines()]}
  
  #part 1
  pairs = Counter([template[i:i+2] for i in range(len(template) - 1)])
  counts = Counter(list(template))
  run = True
  steps = 0
  while run is True and steps < 1000:
    cur = [(p,c) for p,c in pairs.items()]
    for p,c in cur:
      mid = rulebook.get(p, None)
      if mid is None:
        print("Pair not found in rulebook: ", p)
        continue
      pairs.subtract({p: c})
      pairs.update({p[0] + mid: c, mid + p[1]: c})
      counts.update({mid: c})
    steps += 1
    if steps == 10:
      print("Part 1: ", max(counts.values()) - min(counts.values()))
      #run = False  
  #part 2
    if steps == 40:
      print("Part 2: ", max(counts.values()) - min(counts.values()))
      run = False
      
  t1 = time.perf_counter()
  print(t1-t0)
  