# -*- coding: utf-8 -*-
import time
from common import fetchinput
from heapq import heappush, heappop

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  grid = [[int(v) for v in l] for l in raw.splitlines()]
  xdim = len(grid[0])
  ydim = len(grid)
  
  neighbors = [(0,1), (1, 0), (0,-1), (-1, 0)]
  
  #part 1
  start = (0,0)
  goal = (ydim-1, xdim-1)
  heap = []
  heappush(heap, (0, start))
  risk = {start: 0}
  #path = {start: None}
  while len(heap) > 0:
    cur = heappop(heap)[1]
    if cur == goal:
      break
    y,x = cur
    for ny,nx in neighbors:
      fy = y + ny
      fx = x + nx
      if 0 <= fy < ydim and 0 <= fx < xdim:
        nxt = (fy, fx)
        if nxt not in risk or risk[cur] + grid[fy][fx] < risk[nxt]:
          risk[nxt] = risk[cur] + grid[fy][fx]
          pri = risk[nxt] + abs(goal[0] - fy) + abs(goal[1] - fx)
          heappush(heap, (pri, nxt))
          #path[nxt] = cur
  print("Part 1: ", risk[goal])
  
  #part 2
  start = (0,0)
  goal = ((ydim*5)-1, (xdim*5)-1)
  heap = []
  heappush(heap, (0, start))
  risk = {start: 0}
  #path = {start: None}
  while len(heap) > 0:
    cur = heappop(heap)[1]
    if cur == goal:
      break
    y,x = cur
    for ny,nx in neighbors:
      fy = y + ny
      fx = x + nx
      if 0 <= fy < ydim*5 and 0 <= fx < xdim*5:
        wy = fy % ydim
        wx = fx % xdim
        ywrap = int(fy / ydim)
        xwrap = int(fx / xdim)
        nxtrisk = (grid[wy][wx] + ywrap + xwrap) % 9 or 9
        nxt = (fy, fx)
        if nxt not in risk or risk[cur] + nxtrisk < risk[nxt]:
          risk[nxt] = risk[cur] + nxtrisk
          pri = risk[nxt] + abs(goal[0] - fy) + abs(goal[1] - fx)
          heappush(heap, (pri, nxt))
          #path[nxt] = cur
  print("Part 2: ", risk[goal])        
    
  t1 = time.perf_counter()
  print(t1-t0)
  