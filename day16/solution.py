# -*- coding: utf-8 -*-
import time
from common import fetchinput
from collections import deque
from packet import Packet

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  q = deque(list(raw))
  
  #part 1
  packets = Packet()
  packets.parse(q)
  print("Part 1: ", packets.versionSum())
  #part 2
  print("Part 2: ", packets.value)
  t1 = time.perf_counter()
  print(t1-t0)
  