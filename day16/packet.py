# -*- coding: utf-8 -*-
from math import prod

class Packet:
  def __init__(self):
    self.version = None
    self.typeID = None
    self.type = None
    self.packets = []
    self.value = None
    
    self.buffer = 0
    self.bitcount = 0
    self.bitsread = 0
          
  def initBuffer(self, buffer, bitcount):
    self.buffer = buffer
    self.bitcount = bitcount
    
  def readBuffer(self, data, bits):
    while self.bitcount < bits:
      nibble = int(data.popleft(), 16)
      self.buffer <<= 4
      self.buffer |= nibble
      self.bitcount += 4
    val = self.buffer >> self.bitcount - bits
    self.buffer &= 2**(self.bitcount - bits) - 1
    self.bitcount -=  bits
    self.bitsread += bits
    return val
  
  def parse(self, data):
    if (self.version is None or self.typeID is None):
      self.parseHeader(data)
    if self.type == "literal":
      self.parseLiteral(data)
    if self.type == "operator":
      self.parseOperator(data)
    return self.bitsread
  
  def parseHeader(self, data):
    self.version = self.readBuffer(data, 3)
    self.typeID = self.readBuffer(data, 3)
    self.type = "literal" if self.typeID == 4 else "operator"
    
  def parseLiteral(self, data):
    literal = 0
    keepreading = 1
    while keepreading:
      batch = self.readBuffer(data, 5)
      keepreading = batch >> 4
      literal |= batch & 0xF
      if keepreading:
        literal <<= 4
    self.value = literal
    
  def parseOperator(self, data):
    lengthID = self.readBuffer(data, 1)
    if lengthID == 0:
      maxlength = self.readBuffer(data, 15)
      
      startbuf = self.buffer
      startcount = self.bitcount
      while maxlength > 0:
        subpacket = Packet()
        subpacket.initBuffer(startbuf, startcount)
        subpacket.parse(data)
        self.bitsread += subpacket.bitsread
        maxlength -= subpacket.bitsread
        startbuf = subpacket.buffer
        startcount = subpacket.bitcount
        self.packets.append(subpacket)
      if maxlength < 0:
        print("operator parse error: length larger than maxlength")
    elif lengthID == 1:
      maxpackets = self.readBuffer(data, 11)
      startbuf = self.buffer
      startcount = self.bitcount
      i = 0
      while i < maxpackets:
        i += 1
        subpacket = Packet()
        subpacket.initBuffer(startbuf, startcount)
        subpacket.parse(data)
        self.bitsread += subpacket.bitsread
        startbuf = subpacket.buffer
        startcount = subpacket.bitcount
        self.packets.append(subpacket)
      if len(self.packets) != maxpackets:
        print("operator parse error: packet count not as expected")
    self.buffer = startbuf
    self.bitcount = startcount
    vals = [s.value for s in self.packets]
    if self.typeID == 0: #sum
      self.value = sum(vals)
    elif self.typeID == 1: #prod
      self.value = prod(vals)
    elif self.typeID == 2: #min
      self.value = min(vals)
    elif self.typeID == 3: #max
      self.value = max(vals)
    elif self.typeID == 5: #greater than
      a,b = vals
      self.value = int(a > b)
    elif self.typeID == 6: #less than
      a,b = vals
      self.value = int(a < b)
    elif self.typeID == 7: #equals
      a,b = vals
      self.value = int(a == b)
      
  def versionSum(self):
    subversions = sum([s.versionSum() for s in self.packets])
    return subversions + self.version
