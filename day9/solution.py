# -*- coding: utf-8 -*-
import time
from common import fetchinput
from collections import deque
from math import prod 

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  grid = [[int(v) for v in l] for l in raw.splitlines()]
  xdim = len(grid[0])
  ydim = len(grid)
  
  #part 1
  minima = {}
  for y in range(ydim):
    for x in range(xdim):
      v = grid[y][x]
      minimum = True
      if y > 0:
        minimum &= grid[y-1][x] > v
      if y < ydim - 1:
        minimum &= grid[y+1][x] > v
      if x > 0:
        minimum &= grid[y][x-1] > v
      if x < xdim -1:
        minimum &= grid[y][x+1] > v
      if minimum:
        minima[(y,x)] = v + 1
  print("Part 1: ", sum(minima.values()))
    
  
  #part 2
  basins = []
  path = deque()
  for m in minima.keys():
    path.append(m)
    basin = {}
    while len(path) > 0:
      p = path.pop()
      y,x = p
      if p not in basin:
        basin[p] = True
      if y > 0 and (y-1,x) not in basin and grid[y-1][x] < 9:
          path.append((y-1,x))
      if y < ydim - 1 and (y+1,x) not in basin and grid[y+1][x] < 9:
          path.append((y+1,x))
      if x > 0 and (y,x-1) not in basin and grid[y][x-1] < 9:
          path.append((y,x-1))
      if x < xdim -1 and (y,x+1) not in basin and grid[y][x+1] < 9:
          path.append((y,x+1))
    basins.append(sum(basin.values()))
  print("Part 2: ", prod(sorted(basins)[-3:]))
  
  t1 = time.perf_counter()
  print(t1-t0)
  