# -*- coding: utf-8 -*-
import time
from common import fetchinput
from collections import deque
from math import floor

corruptvalue = {
  ")": 3,
  "]": 57,
  "}": 1197,
  ">": 25137,
  }

completionvalue = {
  ")": 1,
  "]": 2,
  "}": 3,
  ">": 4,
  }

leftfromright = {
  ")": "(",
  "]": "[",
  "}": "{",
  ">": "<",
  }

rightfromleft = {
  "(": ")",
  "[": "]", 
  "{": "}", 
  "<": ">", 
  }

leftbrackets = set("{[(<")
rightbrackets = set("}])>")

def checkCorruption(l):
  stack = deque()
  for c in reversed(l):
    if c in rightbrackets:
      stack.append(c)
    if c in leftbrackets:
      if len(stack) > 0:
        rightbracket = stack.pop()
        if rightfromleft[c] != rightbracket:
          return rightbracket
  
def checkCompletion(l):
  stack = deque()
  comp = []
  for c in reversed(l):
    if c in rightbrackets:
      stack.append(c)
    if c in leftbrackets:
      if len(stack) > 0:
        stack.pop()
        continue
      comp.append(rightfromleft[c])
  return "".join(comp)
  
fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  #part 1
  corrupt = []
  incomplete = []
  for line in raw.splitlines():
    c = checkCorruption(line)
    if c is not None:
      corrupt.append(c)
    else:
      incomplete.append(line)
  print("Part 1: ", sum([corruptvalue[c] for c in corrupt]))
    
  #part 2
  completion = []
  for line in incomplete:
    c = checkCompletion(line)
    if c is not None:
      completion.append(c)
  scores = []
  for v in completion:
    score = 0
    for c in v:
      score *= 5
      score += completionvalue[c]
    scores.append(score)
  print("Part 2: ", sorted(scores)[floor(len(scores) / 2)])
  
  t1 = time.perf_counter()
  print(t1-t0)
  