# -*- coding: utf-8 -*-
import time
from common import fetchinput

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  val = [int(v) for v in raw.split(",")]
  
  #part 1
  start = 0
  end = max(val)
  cost = None
  while start != end:
    i = round((start + end) / 2)
    cost = sum([abs(i - v) for v in val])
    r = sum([abs((i + 1) - v) for v in val])
    l = sum([abs((i - 1) - v) for v in val])
    if  cost < r:
      end = min(max(val), i)
    if cost < l:
      start = max(0, i)
  print("Part 1: ", cost)
  
  #part 2
  start = 0
  end = max(val)
  cost = None
  while start != end:
    i = round((start + end) / 2)
    cost = sum([int((abs(i - v) * (abs(i - v) + 1)) / 2) for v in val])
    r = sum([(int(abs((i + 1) - v) * (abs((i + 1) - v) + 1)) / 2) for v in val])
    l = sum([(int(abs((i - 1) - v) * (abs((i - 1) - v) + 1)) / 2) for v in val])
    if  cost < r:
      end = min(max(val), i)
    if cost < l:
      start = max(0, i)
  print("Part 2: ", cost)
    
  t1 = time.perf_counter()
  print(t1-t0)
  