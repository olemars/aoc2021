# -*- coding: utf-8 -*-
import time
from common import fetchinput

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  
  patterns = []
  displays = []
  for v in [l.split(" | ") for l in raw.split("\n")]:
    patterns.append(sorted(["".join(sorted(p)) for p in v[0].split(" ")], key=len))
    displays.append(["".join(sorted(d)) for d in v[1].split(" ")])
    
  #part 1
  lens = [2,3,4,7]
  answer = sum([True for d in displays for v in d if len(v) in lens])
  print("Part 1: ", answer)
  
  #part 2
  #p is sorted by length and lengths 2,3,4,7 are known to correspond to 1,7,4,8
  #lenghts will always be 2,3,4,5,5,5,6,6,6,7
  #p[0] is 1, p[1] is 7, p[2] is 4 and p[9] is 8
  #2,3,5 are length 5
  #0,6,9 are length 6
  outputs = []
  for i,p in enumerate(patterns):
    patterndict =  {}
    patterndict[p[0]] = "1"
    patterndict[p[1]] = "7"
    patterndict[p[2]] = "4"
    patterndict[p[9]] = "8"
   
    wire5 = p[3:6]
    wire6 = p[6:9]
  
    while len(wire5) > 0:
      d = wire5.pop()
      if len(set(d).intersection(p[0])) == 2: #only 3 contains all of 1
        patterndict[d] = "3"
      elif len(set(d).intersection(set(p[2]).difference(p[0]))) == 2: #only 5 contains the diff of 4 and 1
        patterndict[d] = "5"
      else: #last one must be 2
        patterndict[d] = "2"
       
    while len(wire6) > 0:
      d = wire6.pop()
      if len(set(d).intersection(p[2])) == 4: #only 9 contains all of 4
        patterndict[d] = "9"
      elif len(set(d).intersection(p[0])) == 2: #if not 9 then only 0 has all of 1
        patterndict[d] = "0"
      else: #last one must be 6
        patterndict[d] = "6"
    display = "".join([patterndict[d] for d in displays[i]])
    outputs.append(int(display))
  print("Part 2: ", sum(outputs))
  
  t1 = time.perf_counter()
  print(t1-t0)
  