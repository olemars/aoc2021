# -*- coding: utf-8 -*-
import time
from common import fetchinput

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  rawfish = [int(v) for v in raw.split(",")]
  
  #part 1
  fish = rawfish[:]
  fishdict = {}
  for i in range(9):
    fishdict[i] = fish.count(i)
  births = 0
  for d in range(80):
    for i in range(9):
      if i == 0:
        births = fishdict[0]
      else:
        fishdict[i-1] = fishdict[i]
    fishdict[6] += births
    fishdict[8] = births
  print("Part 1: ", sum(fishdict.values()))
    
  #part 2
  fish = rawfish[:]
  fishdict = {}
  for i in range(9):
    fishdict[i] = fish.count(i)
  births = 0
  for d in range(256):
    for i in range(9):
      if i == 0:
        births = fishdict[0]
      else:
        fishdict[i-1] = fishdict[i]
    fishdict[6] += births
    fishdict[8] = births
  print("Part 2: ", sum(fishdict.values()))
    
  t1 = time.perf_counter()
  print(t1-t0)
  