# -*- coding: utf-8 -*-

import os
import requests

def fetchinput():
    if os.path.isfile("input.txt") and os.path.getsize("input.txt") > 0:
        print("input file already exists")
        return
            
    if not os.path.isfile("../session.ini"):
        print("session file missing")
        return
    
    cwd = os.getcwd()
    if len(cwd) > 0 and len(cwd.split("\\")) > 0:
        top = cwd.split("\\")[-1]
        if len(top) > 0:
            day = top[3:]
            with open("../session.ini") as sessionfile:
                session = sessionfile.read().strip()
                r = requests.get("https://adventofcode.com/2021/day/" + day + "/input", cookies={"session": session})
                if r.status_code == 200:
                    with open("input.txt", "w") as inputfile:
                        print("writing input file")
                        inputfile.write(r.text)
                        return
                else:
                    print("error code: ", r.status_code)
                    