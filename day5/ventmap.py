# -*- coding: utf-8 -*-

class VentMap:
  def __init__(self, xmax, ymax):
    self.xmax = xmax + 1
    self.ymax = ymax + 1
    self.vents = [0] * (xmax + 1) * (ymax + 1)
    
  def addline(self, start, end, diags = None):
    if diags is None:
      diags = False
    xrange = end[0] - start[0]
    yrange = end[1] - start[1]
    if diags is False and not (xrange == 0 or yrange == 0):
      return
    maxrange = max(abs(xrange), abs(yrange), 1)
    for d in range(maxrange + 1):
      x = start[0] + round((d * xrange/maxrange))
      y = start[1] + round((d * yrange/maxrange))
      self.vents[(y * self.xmax) + x] += 1    
    
  def viz(self):
    for l in range(self.ymax):
      s = ""
      for v in self.vents[self.xmax * l: self.xmax * (l + 1)]:
        s += str(v) if v > 0 else "."
        s += " "
      print(s)
      
  def getmap(self):
    m = []
    for l in range(self.ymax):
      m.append([v for v in self.vents[self.xmax * l: self.xmax * (l + 1)]])
    return m
      
  def sum(self):
    return sum([v >= 2 for v in self.vents])