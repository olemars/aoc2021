# -*- coding: utf-8 -*-
import time
from common import fetchinput
from ventmap import VentMap
#from matplotlib import pyplot as plt

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  
  xmax = 0
  ymax = 0
  starts  = []
  ends = []
  lines = [l.split(" -> ") for l in raw.splitlines()]
  for l in lines:
    s = l[0].split(",")
    e = l[1].split(",")
    xmax = max(xmax, int(s[0]), int(e[0]))
    ymax = max(ymax, int(s[1]), int(e[1]))
    starts.append((int(s[0]), int(s[1])))
    ends.append((int(e[0]), int(e[1])))    
    
  #part 1
  vents = VentMap(xmax, ymax)
  for s,e in zip(starts, ends):
    vents.addline(s, e)
  print("Part 1: ", vents.sum())
  #m = vents.getmap()
  #plt.imshow(m)
  
  #part 2
  vents = VentMap(xmax, ymax)
  for s,e in zip(starts, ends):
    vents.addline(s, e, True)
  print("Part 2: ", vents.sum())
  #m2 = vents.getmap()  
  #plt.imshow(m2)
  
  t1 = time.perf_counter()
  print(t1-t0)
  