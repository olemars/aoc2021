# -*- coding: utf-8 -*-
import time
from common import fetchinput

rt = {}
def twice(path):
  p = "".join(sorted(path[1:]))
  check = rt.get(p)
  if check is None:
    check = sum([path.count(v) >= 2 for v in path if v.lower() == v]) > 0
    rt[p] = check
  return check

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  connectors = [tuple(l.split("-")) for l in raw.splitlines()]
  
  caves = {}
  for s,e in connectors:
    if s not in caves:
      caves[s] = [e]
    elif e not in caves[s]:
        caves[s].append(e)
    if e not in caves:
      caves[e] = [s]
    elif s not in caves[e]:
        caves[e].append(s)
    
  #part 1
  paths1 = []
  def visit1(cave, curpath):
    curpath.append(cave)
    if cave == "end":
      paths1.append(curpath[:])
      return
    for c in caves[cave]:
      if c.upper() == c or c not in curpath:
        visit1(c, curpath[:])
  visit1("start", [])
  print("Part 1: ", len(paths1))
  
  #part 2
  paths2 = []
  def visit2(cave, curpath):
    curpath.append(cave)
    if cave == "end":
      paths2.append(curpath[:])
      return
    for c in caves[cave]:
      if c.upper() == c or c not in curpath:
        visit2(c, curpath[:])
      elif c in curpath and c not in ["start", "end"] and not twice(curpath):
        visit2(c, curpath[:])
        
  visit2("start", [])
  print("Part 2: ", len(paths2))
  
  t1 = time.perf_counter()
  print(t1-t0)
  