# -*- coding: utf-8 -*-

class BingoTable:
  def __init__(self, data, numcol, numrow):
    self.numcol = numcol
    self.numrow = numrow
    self.table = data
    self.hits = [False] * len(data)
    self.bingo = False
    
  def draw(self, num):
    bingo = False
    if num in self.table:
      self.hits[self.table.index(num)] = True
      bingo = self.checkrows()
      if not bingo:
        bingo = self.checkcols()
    self.bingo = bingo
    return bingo
  
  def tablesum(self):
    return sum([v*(not h) for v,h in zip(self.table, self.hits)])
    
  def checkrows(self):
    for r in range(self.numrow):
      row = self.hits[self.numcol * r: self.numcol * (r + 1)]
      if sum(row) == self.numcol:
        return True
    return False
  
  def checkcols(self):
    for c in range(self.numcol):
      col = self.hits[c: : self.numcol]
      if sum(col) == self.numrow:
        return True
    return False
  
  def isbingo(self):
    return self.bingo