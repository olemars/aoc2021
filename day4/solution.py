# -*- coding: utf-8 -*-
import time
from common import fetchinput
from bingotable import BingoTable

numcol = 5
numrow = 5

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  #raw = infile.read().strip()
  draws = [int(v) for v in infile.readline().split(",")]
  infile.readline()
  rawtables = infile.read().strip().replace("  ", " ")
  tables = []
  for t in rawtables.split("\n\n"):
    rows = [r.strip().split(" ") for r in t.split("\n")]
    vals = [int(v) for r in rows for v in r]
    tables.append(BingoTable(vals, numcol, numrow))
  
  #part 1
  answer = None
  for v in draws:
    for t in tables:
      if t.draw(v) is True:
        answer = v * t.tablesum()
        break
    if answer is not None:
      break
  print("Part 1: ", answer)
        
  #part 2
  vlast = None
  for v in draws:
    vlast = v
    for t in tables:
      t.draw(v)
    if len(tables) > 1:
      tables = [t for t in tables if not t.isbingo()]
    if len(tables) == 1 and tables[0].isbingo():
      break
  print("Part 2: ", vlast * tables[0].tablesum())
    
  t1 = time.perf_counter()
  print(t1-t0)
  