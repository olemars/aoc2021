# -*- coding: utf-8 -*-
import time
from common import fetchinput
from matplotlib import pyplot as plt

fetchinput()
with open("input.txt") as infile:
  t0 = time.perf_counter()
  raw = infile.read().strip()
  rawcoords, rawfolds = raw.split("\n\n")
  xdim = 0
  ydim = 0
  coords = [(int(x), int(y)) for x,y in [l.split(",") for l in rawcoords.splitlines()]]
  for x,y in coords:
    xdim = max(xdim, x+1)
    ydim = max(ydim, y+1)  
  folds = []
  for f in rawfolds.splitlines():
    e = f.index("=")
    folds.append((f[e-1:e], int(f[e+1:])))
    
  grid = [[False for x in range(xdim)] for y in range(ydim)]
  for x,y in coords:
    grid[y][x] = True
    
  #part 1
  foldaxis = "y" 
  count = 0
  for a,f in folds:
    count += 1
    if foldaxis != a:
      foldaxis = a
      grid = list(map(list, zip(*grid)))
    for r in range(len(grid) - f):
      grid[f - r] = [a | b for a,b in zip(grid[f - r], grid[f + r])]
    grid = grid[:f]
    if count == 1:
      print("Part 1: ", sum([v for y in grid for v in y]))
  
  #part 2
  if foldaxis != "y":
      foldaxis = "y"
      grid = list(map(list, zip(*grid)))
  plt.imshow(grid)
  
  t1 = time.perf_counter()
  print(t1-t0)
  